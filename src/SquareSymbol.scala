import Types.Symbol

object SquareSymbol extends Enumeration {
  val BLANK = '·'
  val HIDDEN = '█'
  val BOMB = '¤'
  val FLAG = '¶'
  val ONE = '1'
  val TWO = '2'
  val THREE = '3'
  val FOUR = '4'
  val FIVE = '5'
  val SIX = '6'
  val SEVEN = '7'
  val EIGHT = '8'

  def intToSymbol(value: Int): Symbol = {
    if (value > 0) value.toString.charAt(0)
    else BLANK
  }

}