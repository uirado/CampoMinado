import GameStatus.GameStatus
import Types._
import SquareSymbol._
import Util._

import scala.Console.{WHITE}
import scala.util.Random

class MineField(level: Int) {

  private val size = getLevelInfo._2
  private val matrix: Matrix[Square] = List.tabulate(size, size)((i, j) => new Square(i, j))
  private val allSquares = matrixToList(matrix)
  private var firstTime = true

  def getAllSquares: Squares = allSquares

  def getMatrix: Matrix[Square] = matrix

  def getLevelInfo : LevelInfo  = level match {
    // (level, size, qtdBombs, maxNeighbors)
    case 3 => (level, 26, 80, 7)
    case 2 => (level, 16, 40, 5)
    case _ => (1, 9, 10, 3)
  }

  def getBorder: Squares = {
    allSquares.filter(s => s.line == 0 || s.line == size -1 || s.column == 0 || s.column == size-1)
  }

  def placeBombs(square: Square) : MineField = {
    val (level, _, totalBombs, maxNeighbors) = getLevelInfo
    var restBombs = totalBombs

    if (level != 3) {
      val border = getBorder.diff(square :: getNeighbors(square))
      fieldIterator(border, totalBombs / 2, maxNeighbors)
      restBombs -= totalBombs / 2
    }

    val restField = allSquares.filter(s => !s.hasBomb).diff(square :: getNeighbors(square))
    fieldIterator(restField, restBombs, maxNeighbors)

    this
  }

  def fieldIterator(squareList: Squares, bombs: Int, maxNeighbors: Int): Unit = {
    if(bombs > 0 && squareList.nonEmpty) {
      val i = Random.nextInt(squareList.size)
      val square = squareList(i)

      if (canPlaceBomb(square, maxNeighbors)) {
        square.setValue(BOMB)
        fieldIterator(squareList.diff(List(square)), bombs-1, maxNeighbors)
      } else
        fieldIterator(squareList.diff(List(square)), bombs, maxNeighbors)
    }
  }

  def canPlaceBomb(square: Square, maxNeighbors: Int): Boolean = {
    for(v <- getNeighbors(square))
      if (getNeighbors(v).count(s => s.hasBomb) >= maxNeighbors)
        return false
    true
  }

  def getSquare(line: Int, column: Int): Square = matrix(line)(column)

  def getNeighbors(square: Square): Neighbors = {
    val (line, column) = square.getPosition

    for (s <- this.allSquares;
         (i, j) = s.getPosition
         if (i == line     && j == column - 1)  ||
            (i == line     && j == column + 1)  ||
            (i == line - 1 && j == column - 1)  ||
            (i == line - 1 && j == column    )  ||
            (i == line - 1 && j == column + 1)  ||
            (i == line + 1 && j == column - 1)  ||
            (i == line + 1 && j == column    )  ||
            (i == line + 1 && j == column + 1))
      yield s
  }

  def enumerateSquares(): Squares = {
    val fieldsNotBomb = this.allSquares.filter(square => !square.hasBomb)
    enumerate(square => getNeighbors(square).count(s => s.hasBomb), fieldsNotBomb)
  }

  def enumerate(countNeighbors: Square => Int, fields: Squares): Squares = fields match {
    case Nil => Nil
    case square :: tail =>
      val newValue = intToSymbol(countNeighbors(square))
      square.setValue(newValue) :: enumerate(countNeighbors, tail)
  }

  def revealExpand(square: Square): Square = {
    square.getState match {
      case (SquareSymbol.BLANK, true, false) =>
        if(firstTime) {
          placeBombs(square).enumerateSquares()
          firstTime = false
        }
        square.reveal
        getNeighbors(square).foreach(s => revealExpand(s))
        square
      case (SquareSymbol.BOMB, true, false) => square.boom
      case (_, true, true) => square
      case _ => square.reveal
    }
  }

  def endGame(endStatus: GameStatus): Unit = {
    endStatus match {
      case GameStatus.LOSE => revealAll(allSquares.filter(s => !s.isFlagged || s.hasBomb))
      case GameStatus.WIN => allSquares.filter(s => s.hasBomb).foreach(s => s.setFlag())
    }
    printField()
  }

  def checkWin(): Boolean = allSquares.count(s => s.isHidden) == this.getLevelInfo._3


  def revealAll(squareList: Squares): Squares = squareList.map(s => s.reveal)

  def revealAll: MineField = {allSquares.map(s => s.reveal); this}

  def revealBombs: Squares = revealAll(this.allSquares.filter(s => s.hasBomb))

  def countRemainingBombs(): Int = {
    if(firstTime) getLevelInfo._3
    else allSquares.count(s => s.hasBomb) - allSquares.count(s => s.isFlagged)
  }

  def printField(): Unit = {

    val (level, size, qtdBombs, _) = getLevelInfo
    println("\n    " + fill("#", (2*size-12)/2) + " CAMPO  MINADO " + fill("#", (2*size-12)/2) + "\n")

    //imprime linha com as letras de A a ('A' + size)
    println(WHITE + "      " + makeLetterSeq(size))

    //borda superior
    print("    ╔"); for(_ <- 0 until 2*this.size + 1) print('═'); println('╗')

    //numeração das linhas, bordas laterais e matriz

    for(i <- 0 until size) {
      val line = matrix(i)
      print(WHITE + format2digitsR(i + 1) + " ║ " + lineToString(line) + "║" + format2digitsL(i + 1) + "   ")

      if (i == 0) print("[ Nível " + getLevelName(level) + " ]")
      if (i == 1) print("[ Tamanho: " + size + "x" + size + " ]")
      if (i == size - 2) print("[ Quadrados restantes: " + allSquares.count(s => s.isHidden && !s.isFlagged) + " ]")
      if (i == size - 1) print("[ Bombas restantes: " + countRemainingBombs() + " ]")

      println(WHITE)
    }

    //borda inferior
    print("    ╚"); for(_ <- 0 until 2*this.size + 1) print('═'); println('╝')

    //imprime linha com as letras de A a ('A' + size)
    println(WHITE + "      " + makeLetterSeq(size))
    println(fill("─", size*2 + 10))

  }

}
