import Types.{Letter, LetterSeq, Line, Matrix}

import scala.util.Random

object Util {

  def getLevelName(level: Int): String = level match {
    case 1 => "Fácil"
    case 2 => "Médio"
    case 3 => "Difícil"
  }

  def format2digitsR(n: Int): String = f" $n%2d"
  def format2digitsL(n: Int): String = f"$n% 2d"

  //retorna a letra do alfabeto (A a Z) correspondente ao índice passado
  def asciiLetter(i: Int): Letter = ('A' + i).toChar

  def fill(str: String, n: Int): String = n match {
    case 0 => ""
    case k =>
      if (k < 0) ""
      else str + fill(str, k - 1)
  }

  //retorna uma String com as letras do alfabeto(A a Z) correspondente a quantidade passada
  def makeLetterSeq(size: Int): LetterSeq = size match {
    case 0 => ""
    case n => makeLetterSeq(n - 1) + asciiLetter(n - 1) + " "
  }

  def matrixToList[T](matrix: Matrix[T]): Line[T] = matrix match {
    case Nil => Nil
    case head :: tail => head ++ matrixToList(tail)
  }

  def lineToString[T](line: Line[T]): String = line match {
    case Nil => ""
    case square :: tail => square.toString + lineToString(tail)
  }

  def genRandomIndexes(amount: Int, min: Int, max: Int): List[Int] = {
    var s: Set[Int] = Set()
    val r = new Random()

    while(s.size < amount){
      s = s + (min + r.nextInt(max - min))
    }
    s.toList
  }

}
