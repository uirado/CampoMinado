import CommandType.CommandType

object Types {

  type LineIndex = Int
  type ColumnIndex = Int
  type Position = (LineIndex, ColumnIndex)                // (linha, coluna) de um quadrado
  type IsHidden = Boolean
  type IsFlagged = Boolean
  type State = (Symbol, IsHidden, IsFlagged)   // (value, hidden, flagged) de um quadrado
  type Symbol = Char                        // Símbolos dos possíveis values de um quadrado
  type Letter = Char                        // uma letra
  type LetterSeq = String                   // uma sequência de letras
  type Squares = List[Square]               // Lista com parte dos squares do minefield
  type Neighbors = Squares                  // Lista com os vizinhos de um determinado square
  type Line[T] = List[T]                    // Lista com todos os squares de uma linha da matriz
  type Matrix[T] = List[Line[T]]            // Matrix completa do minefield
  type Level = Int
  type Size = Int
  type QtyBombs = Int
  type MaxNeighbors = Int
  type LevelInfo = (Level, Size, QtyBombs, MaxNeighbors)          // (level, size, qtdBombs) informações do level do minefield
  type ParserResult = (String, Command)
  type Error = String
  type Command = (CommandType, String, Position)

}
