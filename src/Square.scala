import SquareSymbol._
import Types._

class Square(val line: Int, val column: Int) {

  private var value = BLANK
  private var hidden = true
  private var flagged = false
  private var hit = false

  def reveal: Square = {
    hidden = false
    this
  }

  def getPosition: Position = (line, column)

  def getState: State = (value, hidden, flagged)

  def boom: Square = {
    hit = true
    this
  }

  def hasBomb: Boolean = value == BOMB

  def isHidden: Boolean = hidden

  def isFlagged: Boolean = flagged

  def flag: Square = {
    flagged = !flagged
    this
  }
  def setFlag(): Square = {
    flagged = true
    this
  }

  def setValue(newValue: Symbol) : Square = {
    this.value = newValue
    this
  }

  def getValue: Symbol = this.value

  override def toString: String = {
    import Console._
    val color = value match {
      case BOMB => RED
      case BLANK => WHITE
      case ONE => BLUE
      case TWO => GREEN
      case THREE => MAGENTA
      case _ => WHITE
    }

    if (hidden) {
      if (flagged) RED + FLAG + WHITE + " "
      else WHITE + HIDDEN + WHITE + " "
    }
      else if (hit)
        color + BOLD + value + WHITE + " "
    else
        color + BOLD + value + WHITE + " "
  }
}