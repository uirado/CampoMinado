import Types.{ParserResult, Position}
import Console._

class Parser {
  private val ERROR = (CommandType.INVALID_COMMAND, null)
  private val HELP_MESSAGE =
    WHITE + "[ COMANDOS DO JOGO ]\n\n" +
    BLUE + "<linha><coluna>" + WHITE + " ou " + BLUE + "<linha> <coluna>" + WHITE + " ou " + BLUE + "<coluna><linha>" + WHITE + " ou " + BLUE + "<coluna> <linha> \n" +
    WHITE + "Efeito: Jogar na posição escolhida. ( linha[a-zA-Z], coluna[1-26]] )\n" +
    WHITE + "ex: \"3J\", \"3 J\", \"J3\", \"J 3\"\n\n" +
    BLUE + "flag <linha><coluna>" + WHITE + " ou " + BLUE + "flag <linha> <coluna>" + WHITE + " ou " + BLUE + "flag <coluna><linha>" + WHITE + " ou " + BLUE + "flag <coluna> <linha>\n" +
    WHITE + "Efeito: Marcar flag na posição escolhida ] \n" +
    WHITE + "ex: \"flag 3J\", \"flag 3 J\"\n\n" +
    BLUE + "reset" + WHITE + " ou " + BLUE + "reset <level>\n" +
    WHITE + "Efeito: Reinicia o jogo no mesmo level ou no level escolhido. ( level[1-3] ).\n" +
    WHITE + "ex: \"reset\", \"reset 3\"\n\n" +
    BLUE + "help\n" +
    WHITE + "Efeito: Exibe descrição dos comandos. \n\n" +
    BLUE + "exit\n" +
    WHITE + "Efeito: Encerra o jogo.\n"

  def process(command: String) : ParserResult = {
    import CommandType.{EXIT, FLAG, RESET, REVEAL}
    val list: List[String] = command.trim().split(" ").toList

    list match {
      case "" :: Nil => null

      case ("flag") :: i :: j :: Nil =>
        val pos = validatePosition(i, j)
        if (pos != null)  (null, (FLAG, null, pos))
        else ERROR

      case ("flag") :: ij :: Nil =>
        val pos = validatePosition(ij)
        if (pos != null)  (null, (FLAG, null, pos))
        else ERROR

      case ("reset") :: level :: Nil =>
        if (isNumber(level)) (null, (RESET, level, null))
        else ERROR

      case ("reset") :: Nil => (null, (RESET, null, null))

      case "exit" :: Nil => (null, (EXIT, null, null))

      case "help" :: Nil => println(HELP_MESSAGE); null

      case i :: j :: Nil =>
        val pos = validatePosition(i, j)
        if (pos != null)  (null, (REVEAL, null, pos))
        else ERROR

      case ij :: Nil =>
        val pos = validatePosition(ij)
        if (pos != null)  (null, (REVEAL, null, pos))
        else ERROR

      case _ => ERROR
    }
  }

  def validatePosition(ij: String): Position = {
    if(ij.length == 2)
      return validatePosition(ij.charAt(0).toString, ij.charAt(1).toString)
    else if (ij.length == 3)
      return  validatePosition(ij.charAt(0).toString, ij.charAt(1).toString, ij.charAt(2).toString)
    null
  }

  def validatePosition(i: String, j: String): Position = {
    if (isNumber(i) && isLetter(j))
      (getLine(i), getColumn(j) )
    else if(isNumber(j) && isLetter(i))
      (getLine(j), getColumn(i) )
    else null
  }

  def validatePosition(i: String, j: String, k: String): Position = {
    if (isNumber(i+j) && isLetter(k))
      (getLine(i+j), getColumn(k))
    else if(isNumber(k) && isLetter(i+j))
      (getLine(k), getColumn(i+j))
    else if (isNumber(i) && isLetter(j+k))
      (getLine(i), getColumn(j+k))
    else if(isNumber(j+k) && isLetter(i))
      (getLine(j+k), getColumn(i))
    else null
  }

  def getLine(line: String) : Int = line.toInt - 1
  def getColumn(column: String) : Int = getIndexOf(column)

  def getIndexOf (letter: String) : Int = {
    val alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    alphabet.indexOf(letter.toUpperCase)
  }

  def isNumber(token: String) : Boolean = token.matches("[1-9]") || token.matches("[1-9][0-9]")

  def isLetter(token: String) : Boolean = token.matches("[a-zA-Z]")

}

object CommandType extends Enumeration {
  type CommandType = Value
  val FLAG, RESET, REVEAL, EXIT = Value
  val INVALID_COMMAND: String = RED + BOLD + "Comando Inválido." + WHITE + "(digite "+ BLUE + "help" + WHITE + " para ver ajuda)" + WHITE
}