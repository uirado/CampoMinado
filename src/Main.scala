import GameStatus.GameStatus
import Types.{Command, Position}
import CommandType._
import Console.{RED, WHITE, GREEN, BLACK}

import scala.io.StdIn

object Main extends App {
  val OUT_OF_BOUNDS = RED + "As coordenas excedem o tamanho do campo minado" + WHITE
  val INVALID_LEVEL = RED + "Escolha um level entre 1 e 3" + WHITE
  val UNKNOWN_ERROR = RED + "Erro desconhecido" + WHITE
  val WIN_MESSAGE = GREEN +"Parabéns, você venceu o jogo! :D " + WHITE
  val LOSE_MESSAGE = RED + "Você acertou uma mina e perdeu o jogo! :( " + WHITE
  val GAME_OVER = WHITE + "[ FIM DE JOGO ]" + WHITE
  val EXIT_MESSAGE = WHITE + "saindo..." + WHITE

  val parser: Parser = new Parser()

  do {
    startGame()

    playAgain()

  } while(true)

  def playAgain(): Boolean = {
    print("Deseja jogar novamente? (1 - Sim / 2 - Não): ")

    val input = StdIn.readLine()
    if(input.equals("1")) return true
    if(input.equals("2")) exit()

    println(INVALID_COMMAND)
    playAgain()
  }

  def exit(): Unit = {
    println(EXIT_MESSAGE)
    sys.exit()
  }

  def startGame() = {
    var finishGame = false
    var mineField = new MineField(1)

    do {
      mineField.printField()

      execute(getCommand, mineField) match {
        case GameStatus.LEVEL_1 => mineField = new MineField(1)
        case GameStatus.LEVEL_2 => mineField = new MineField(2)
        case GameStatus.LEVEL_3 => mineField = new MineField(3)
        case GameStatus.OK => //continue
        case endStatus =>
          finishGame = true
          mineField.endGame(endStatus)
          println(GAME_OVER)
          endStatus match {
            case GameStatus.LOSE => println(LOSE_MESSAGE)
            case GameStatus.WIN => println(WIN_MESSAGE)
          }
      }
    }
    while(!finishGame)
  }

  def getCommand: Command = {
      print("Digite um comando: ")
      val command = StdIn.readLine()
      parser.process(command) match {
        case (error, null) =>
          println(error)
          getCommand
        case (_, command: Command) => command
        case _ => getCommand
      }
  }

  def execute(command: Command, game: MineField): GameStatus = {
    command match {
    case (FLAG, _, position) => executeFlag(position, game); GameStatus.OK

    case (RESET, null, _) => executeReset(game.getLevelInfo._1, game)

    case (RESET, n, _) => executeReset(n.toInt, game)

    case (REVEAL, _, position) => executeReveal(position, game)

    case (EXIT, _, _) => exit(); GameStatus.OK

    case _ => println(UNKNOWN_ERROR); execute(command, game)

    }
  }

  def executeFlag(position: Position, game: MineField): Unit = {
    val (i, j) = position
    if(checkBounds(i, j, game.getLevelInfo._2))
      if(game.getSquare(i, j).isHidden)
        game.getSquare(i, j).flag
      else {
        execute(getCommand, game)
      }
    else {
      println(OUT_OF_BOUNDS)
      execute(getCommand, game)
    }
  }

  def executeReset(level: Int, game: MineField) : GameStatus = level match {
    case 1 => GameStatus.LEVEL_1
    case 2 => GameStatus.LEVEL_2
    case 3 => GameStatus.LEVEL_3
    case _ =>
      println(INVALID_LEVEL)
      execute(getCommand, game)
  }

  def executeReveal(position: Position, game: MineField): GameStatus = {
    val (i, j) = position
    if(checkBounds(i, j, game.getLevelInfo._2))
      if(game.getSquare(i, j).isHidden) {
        if (game.revealExpand(game.getSquare(i, j)).hasBomb) {
          GameStatus.LOSE
        } else {
          if (game.checkWin()) GameStatus.WIN
          else GameStatus.OK
        }
      } else
        execute(getCommand, game)
    else {
      println(OUT_OF_BOUNDS)
      execute(getCommand, game)
    }
  }

  def checkBounds(i: Int, j: Int, size: Int) : Boolean = i < size && j < size

}

object GameStatus extends Enumeration {
  type GameStatus = Value
  val LOSE, WIN, OK, LEVEL_1, LEVEL_2, LEVEL_3 = Value
}

